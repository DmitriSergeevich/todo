import React from "react";
import TodoListPage from "../pages/TodoListPage";
import TodoEditPage from "../pages/TodoEditPage";

export interface iRoute {
  path: string;
  component: React.ComponentType;
}

export enum RouteNames {
  TODO_LIST = "/",
  TODO_EDIT = "/edit",
}

export const routes: iRoute[] = [
  {
    path: RouteNames.TODO_LIST,
    component: TodoListPage,
  },
  {
    path: RouteNames.TODO_EDIT,
    component: TodoEditPage,
  },
];
