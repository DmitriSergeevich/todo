import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,  
  Navigate
} from "react-router-dom";
import { RouteNames, routes } from './router/router';



function App() {
  return (
    <div className="App">
      <div>Header</div>
      <BrowserRouter>
        <Routes>
          {routes.map((e) => (
            <Route key={e.path} path={e.path} element={<e.component />} />
          ))}
          <Route path="*" element={<Navigate to={RouteNames.TODO_LIST} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
